import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Student } from '../models/student';
import { retry, catchError, map } from 'rxjs/operators';

import { HTTP } from '@ionic-native/http/ngx';
import { finalize } from 'rxjs/operators';
import { Observable, from, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  base_path = 'http://jsonplaceholder.typicode.com/users';
  urlApi = 'http://mercadoactivo.cl/apis/';

  constructor(private http: HttpClient, public nativeHttp: HTTP) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

  // Get students data
  getList(): Observable<Student> {
    return this.http
      .get<Student>(this.base_path)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  hacerLogin(email, username){
    return this.nativeHttp.get(this.urlApi+'login.php?email='+email+'&username='+username, {}, { 'Content-Type': 'application/json' });
  }

  listCategoria(){
    return this.http.get(this.urlApi+'categorias.php');
  }

  obtenerDatos(){
    return this.http.get(this.base_path);
  }

  obtenerCarrito(usu){
    return this.http.get(this.urlApi+'carrito.php?correo='+usu);
  }

  borrarItemOrden(correo, iddetalle){
    return this.http.get(this.urlApi+'ordenDetalleDelete.php?correo='+correo+'&detalle='+iddetalle);
  }

  borrarOrden(correo, idorden){
    return this.http.get(this.urlApi+'ordenDelete.php?correo='+correo+'&orden='+idorden);
  }

  listarProductosCategoria(idCategoria){
    return this.http.get(this.urlApi+'categoriasId.php?catId='+idCategoria);
  }

  detalleProducto(idProduct){
    return this.http.get(this.urlApi+'detalleId.php?producto='+idProduct);
  }

  anunciosHot(){
    return this.http.get(this.urlApi+'anunciosHot.php?');
  }

  anuncios(pag){
    return this.http.get(this.urlApi+'anuncios.php?pag='+pag);
  }

  anunciosTodos(){
    return this.http.get(this.urlApi+'anunciosTodos.php');
  }

  comprarProducto(idProduct, correo){
    return this.http.get(this.urlApi+'compra.php?correo='+correo+'&producto='+idProduct+'&cantidad=1');
  }

  obtenerCompras(correo){
    return this.http.get(this.urlApi+'compras.php?correo='+correo);
  }

  cambiarStatus(correo, producto, status){
    return this.http.get(this.urlApi+'actualizarCompra.php?correo='+correo+'&producto='+producto+'&status='+status);
  }

  guardarCalificacion(correo, producto, puntaje){
    return this.http.get(this.urlApi+'calificacion.php?correo='+correo+'&producto='+producto+'&puntaje='+puntaje);
  }

}
