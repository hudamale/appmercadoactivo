import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  public sessionId;
  constructor(private route: Router, public alertController: AlertController){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      this.sessionId = localStorage.getItem('user_login');
      if(this.sessionId != "null"){
        return true;
      }else{
        this.mensaje();     
        this.route.navigate(['tabs/tab1']);      
        return false;
      }
  }

  async mensaje(){
    const alert = await this.alertController.create({
      header: 'Debe logearse',
      message: 'Para interactuar en la aplicación',
      buttons: ['OK']
    });

    await alert.present();
  }
  
}
