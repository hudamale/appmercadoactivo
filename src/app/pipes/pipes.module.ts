import { NgModule } from '@angular/core';
import { FiltroPipe } from './filtro.pipe';
import { FiltrodetallePipe } from './filtrodetalle.pipe';

@NgModule({
  declarations: [FiltroPipe, FiltrodetallePipe],
  exports: [FiltroPipe, FiltrodetallePipe]
})
export class PipesModule { }
