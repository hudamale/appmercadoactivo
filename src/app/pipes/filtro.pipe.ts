import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform( arreglo: any[], texto: string): any[] {    
    
    if(texto.length === 0){
      return arreglo;
    }

    texto = texto.toLowerCase();

    //return arreglo.filter(p => p.datos.some(s => s.product_name.toLowerCase().indexOf( texto ) > -1));

    return arreglo.filter( item => {
      return item.nombre_producto.toLowerCase().indexOf( texto ) > -1 ;
    });
    
  }

}
