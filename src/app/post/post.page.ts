import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../services/api.service';
import { ToastController, LoadingController } from '@ionic/angular';

import { HTTP } from '@ionic-native/http/ngx';
import { finalize } from 'rxjs/operators';
import { Observable, from } from 'rxjs';

@Component({
  selector: 'app-post',
  templateUrl: './post.page.html',
  styleUrls: ['./post.page.scss'],
})
export class PostPage implements OnInit {

  id;
  posts : any = [];
  list;
  urlApi = 'https://mercadoactivo.cl';
  textoBuscar = '';

  constructor(  public router: Router, 
                private route: ActivatedRoute, 
                public apiService: ApiService,
                public nativeHttp: HTTP,
                public loadinctrl: LoadingController) { 
    this.id = this.route.snapshot.paramMap.get('postId');
  }

  ngOnInit() {
    this.listarProductos(this.id);
  }

  async listarProductos(idCategoria){
    let cargandoInf = await this.loadinctrl.create({
      message: 'Buscando ...'
    });
    await cargandoInf.present();
    console.log('Buscando inf: '+idCategoria);
    
    /*this.apiService.listarProductosCategoria(idCategoria).subscribe( res =>
      {
        this.list = res; 
        for (let post of this.list) {
          let foto = this.urlApi+post["link"]+post["fotos"].split(",",1);          
          post.media_url = foto;
          post["descripcion"] = post["descripcion"].replace("<p","");
          post.descripcion = post["descripcion"].substring(1,208) + ' ...';
        }
        this.posts = this.list;
        cargandoInf.dismiss();
      })*/

      let buscandoProd = this.nativeHttp.get(localStorage.getItem("URLAPI")+'categoriasId.php?catId='+idCategoria, {}, {'Content-Type': 'application/json'});
        from(buscandoProd).pipe(
            finalize(() => console.log('termino'))
          ).subscribe(data => {
            this.list = JSON.parse(data.data);
            for (let post of this.list) {
              let foto = this.urlApi+post["link"]+post["fotos"].split(",",1);          
              post.media_url = foto;
              post["descripcion"] = post["descripcion"].replace("<p","");
              post.descripcion = post["descripcion"].substring(1,208) + ' ...';
            }
            this.posts = this.list;
            cargandoInf.dismiss();
          }, err =>{
            console.log('No signal');
          })
  }

  regresar(){
    this.router.navigate(['/tabs/tab2'],{replaceUrl: false});  
  }

  buscarProducto( event ){
    this.textoBuscar = event.target.value;
    let buscar = this.textoBuscar.toLowerCase();
  }

}
