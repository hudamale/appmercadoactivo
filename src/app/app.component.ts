import { Component } from '@angular/core';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { OneSignal } from '@ionic-native/onesignal/ngx';
import { HttpClientModule } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  public appPages = [
    {
      title: 'Perfil',
      url: 'datos',
      icon: 'construct'
    },
    {
      title: 'Listado',
      url: 'tabs/tab2',
      icon: 'create'
    },
    {
      title: 'Mis anuncios',
      url: 'list',
      icon: 'list'
    },
    {
      title: 'Salir',
      url: 'tabs/tab4',
      icon: 'power'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private onesignal: OneSignal,
    public alertCtrl: AlertController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.onesignal.startInit("a6a77bc6-acc6-43ab-bae7-1d8fc297ab4b","797062725644").endInit();
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.handlerNotifications();
    });
    
  }

  async handlerNotifications(){
    this.onesignal.startInit("a6a77bc6-acc6-43ab-bae7-1d8fc297ab4b", "797062725644");
    this.onesignal.inFocusDisplaying(this.onesignal.OSInFocusDisplayOption.Notification);
    this.onesignal.handleNotificationOpened()
    .subscribe(jsonData => {      
      console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      this.presentAlert(jsonData);
    });
    this.onesignal.endInit();
  }

  async presentAlert(mensaje) {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: mensaje.notification.payload.title,
      //subHeader: mensaje.notification.payload.body,
      message: mensaje.notification.payload.body,
      buttons: ['OK']
    });
    await alert.present();
  }

}
