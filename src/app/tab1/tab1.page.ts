import { Component } from '@angular/core';

import {Validators, FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ApiService } from '../services/api.service';
import { ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

import { HttpClient } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { finalize } from 'rxjs/operators';
import { Observable, from } from 'rxjs';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  todo = {};
  isActive = false;
  datoscarrito;
  orden;
  total;
  error = 0;
  datos;  
  urlApi = 'https://mercadoactivo.cl/apis/';

  constructor(public loadinctrl: LoadingController, 
              public apiService: ApiService,
              public toastController: ToastController,
              public nativeHttp: HTTP,
              public router: Router ) {
                localStorage.setItem('URLAPI', this.urlApi);
  }

  ngOnInit() {
    localStorage.setItem('user_login',null);    
  }  

  async logForm() {
    
    let loading = await this.loadinctrl.create({
      message: 'Cargando ...'
    });

    const faltanDatos = await this.toastController.create({
      message: 'Faltan datos en el formulario',
      duration: 2000
    });

    const sinLogeo = await this.toastController.create({
      message: 'Nombre de usuario o Email no encontrados',
      duration: 2000
    });

    var email = this.todo["email"];
    var username = this.todo["password"];

    if(email == "" || email == undefined)       this.error = 1;
    if(username == "" || username == undefined) this.error = 1;

    if(this.error == 0){
      await loading.present();

      let nativeCall = this.nativeHttp.get(localStorage.getItem("URLAPI")+'login.php?username='+username+'&email='+email, {}, {'Content-Type': 'application/json'});

        from(nativeCall).pipe(
          finalize(() => console.log('termino'))
        )
        .subscribe(data => {
          this.datos = JSON.parse(data.data);
          console.log(' ESTADO:: '+this.datos["success"]);
          loading.dismiss();
          if(this.datos["success"] == true){
            localStorage.setItem('user_login',email);
            this.router.navigate(['/tabs/tab2'],{replaceUrl: false});  
          }else{
            localStorage.setItem('user_login',null);
            sinLogeo.present();
          }
        }, err =>{
          console.log('No signal');
        })            
      
    }else{
      localStorage.setItem('user_login',null);
      faltanDatos.present(); 
    }

  }

  async getDatos(){
      this.apiService.obtenerCarrito('hugomaldonado23@gmail.com').subscribe( res => {
        console.log(res);
        this.datoscarrito = res;
        let totalitem;
        for(let data of this.datoscarrito) {
          this.orden = res[0]['idorden'];
          this.total = res[0]['monto'];
        }
      });
    }    

}
