import { Component, Input } from '@angular/core';
import { ApiService } from '../services/api.service';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';

import { HTTP } from '@ionic-native/http/ngx';
import { finalize } from 'rxjs/operators';
import { Observable, from } from 'rxjs';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  datoscarrito;
  correo: string;
  orden;
  total = 0;
  data: any = [];
  statusNew;

  constructor(public apiService: ApiService,
              public loadinctrl: LoadingController,
              public alertController: AlertController,
              public nativeHttp: HTTP,
              public toastController: ToastController) {}

  ngOnInit() {
    this.getCompras();
  }

  async getCompras(){
    let cargandoInf = await this.loadinctrl.create({
      message: 'Buscando ...'
    });
    await cargandoInf.present();
    this.correo = localStorage.getItem('user_login');
    /*this.apiService.obtenerCompras(this.correo).subscribe( res => {
      this.data = res;
      this.total = this.data.length;
      cargandoInf.dismiss();
    });*/

    let nativeCall = this.nativeHttp.get(localStorage.getItem("URLAPI")+'compras.php?correo='+this.correo, {}, {'Content-Type': 'application/json'});
        from(nativeCall).pipe(
            finalize(() => console.log('termino'))
          ).subscribe(data => {
            this.data = JSON.parse(data.data);
            this.total = this.data.length;
            cargandoInf.dismiss();
          }, err =>{
            console.log('No signal');
          })

  }

  async calificarVendedor(product) {
    this.correo = localStorage.getItem('user_login');
    const alert = await this.alertController.create({
      header: 'Como lo calificarías?',
      inputs: [
        {
          name: 'radio1',
          type: 'radio',
          label: 'Excelente',
          value: '4',
          checked: true
        },
        {
          name: 'radio2',
          type: 'radio',
          label: 'Bueno',
          value: '3'
        },
        {
          name: 'radio3',
          type: 'radio',
          label: 'Regular',
          value: '2'
        },
        {
          name: 'radio4',
          type: 'radio',
          label: 'Malo',
          value: '1'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Cancelar');
          }
        }, {
          text: 'Calificar',
          handler: (data) => {
            console.log('Calificar');
            //console.log(product);
            //console.log(JSON.stringify(data));
            //console.log(data);
            this.guardarCalificacion(this.correo, product, data);
          }
        }
      ]
    });

    await alert.present();
  }

  async guardarCalificacion(correo, producto, puntaje){
    let guardando = await this.loadinctrl.create({
      message: 'Guardando ...'
    });
    await guardando.present();
    /*this.apiService.guardarCalificacion(correo, producto, puntaje).subscribe( res => {
      console.log(res);
      guardando.dismiss();
      this.cambiar(producto, 3);
    });*/
    let guardarCalif = this.nativeHttp.get(localStorage.getItem("URLAPI")+'calificacion.php?correo='+correo+'&producto='+producto+'&puntaje='+puntaje, {}, {'Content-Type': 'application/json'});
        from(guardarCalif).pipe(
            finalize(() => console.log('termino'))
          ).subscribe(data => {
            this.data = JSON.parse(data.data);
            guardando.dismiss();
            this.cambiar(producto, 3);
          }, err =>{
            console.log('No signal');
          })
  }

  async cambiar(product, status){
    if(status == 0) this.statusNew = 1;
    if(status == 1) this.statusNew = 2;
    if(status == 2) this.statusNew = 3;
    if(status == 3) this.statusNew = 4;

    this.correo = localStorage.getItem('user_login');
    let cargandoInf = await this.loadinctrl.create({
      message: 'Cargando ...'
    });
    await cargandoInf.present();
    /*this.apiService.cambiarStatus(this.correo, product, this.statusNew).subscribe( res => {
      cargandoInf.dismiss();
      this.getCompras();      
    });*/
    let cambiar = this.nativeHttp.get(localStorage.getItem("URLAPI")+'actualizarCompra.php?correo='+this.correo+'&producto='+product+'&status='+this.statusNew, {}, {'Content-Type': 'application/json'});
        from(cambiar).pipe(
            finalize(() => console.log('termino'))
          ).subscribe(data => {
            this.data = JSON.parse(data.data);
            cargandoInf.dismiss();
            this.getCompras();
          }, err =>{
            console.log('No signal');
          })
  }

  calificar(product){
    this.calificarVendedor(product);
  }

  getDatos(){
    this.correo = localStorage.getItem('user_login');
    this.apiService.obtenerCarrito(this.correo).subscribe( res => {
      this.datoscarrito = res;
      console.log("paso");
      let totalitem;
      for(let data of this.datoscarrito) {
        this.orden = res[0]['idorden'];
        this.total = res[0]['monto'];
      } 
    });
  }

  borrarDetalleOrden(usu, orden){
    this.apiService.borrarItemOrden(usu, orden).subscribe( res => {
        console.log("borrado");
        this.getDatos();
      });
  }

  borrarOrden(usu, orden){
    this.apiService.borrarOrden(usu, orden).subscribe( res => {
      console.log("borrado");
      this.getDatos();
      this.total = 0;
    });
  }

  update(){
    this.getCompras();  
  }

  async pagar(orden, monto){

    const toast = await this.toastController.create({
      message: 'Debe tener un monto > 0 para ir a pagar ...',
      duration: 2000
    });

     monto = monto.replace(',','.');

    if(monto > 0){
        let montoencode = btoa(""+monto);
        let ordenencode = btoa(""+orden);
        window.open('https://tiendauniversal.cl/pagos/php/paywebpay.php?id='+ordenencode+'&em='+montoencode, '_blank', 'location=yes');
        //this.iab.create('https://tiendauniversal.cl/pagos/php/paywebpay.php?id='+ordenencode+'&em='+montoencode, '_blank');
    }else{
      toast.present();
    }    
  }

}
