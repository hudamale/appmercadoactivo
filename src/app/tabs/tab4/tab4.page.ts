import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

  pasada: any;
  constructor(public router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    if(window.confirm("Desea salir ?")){
      localStorage.setItem('user_login',null);
      navigator["app"].exitApp();
    }else{
      this.router.navigate(['tabs/tab1'],{replaceUrl: false});
      this.pasada++;
    }
  }

}
