import { Component } from '@angular/core';

import { ApiService } from '../services/api.service';
import { ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

import { HTTP } from '@ionic-native/http/ngx';
import { finalize } from 'rxjs/operators';
import { Observable, from } from 'rxjs';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  anunciosTodos : any = [];
  posts : any = [];
  datos;
  token;
  data;
  habilitado;
  textoBuscar = '';
  anunciosHot;
  anunciosHot2;
  arrayFotosHot = [];
  arrayFotos = [];
  fotos = [];
  fotosHot = [];
  urlApi = 'https://mercadoactivo.cl';
  mostrar = true;
  pagina = 1;
  pasada = 0;

  constructor(public loadinctrl: LoadingController, 
    public apiService: ApiService,
    public toastController: ToastController,
    public nativeHttp: HTTP,
    private iab: InAppBrowser,
    private httpClient: HttpClient,
    public router: Router ) 
    
    {      

    }

    async ngOnInit(){

      let loading = await this.loadinctrl.create({
        message: 'Cargando ...'
      });
      
      
    await loading.present();

      let nativeCall = this.nativeHttp.get(localStorage.getItem("URLAPI")+'categorias.php', {}, {'Content-Type': 'application/json'});
        from(nativeCall).pipe(
            finalize(() => console.log('termino'))
          ).subscribe(data => {
            this.datos = JSON.parse(data.data);
            this.posts = JSON.parse(data.data);            
            //loading.dismiss();
          }, err =>{
            console.log('No signal');
          });

        let anunciosHot = this.nativeHttp.get(localStorage.getItem("URLAPI")+'anunciosHot.php', {}, {'Content-Type': 'application/json'});
          from(anunciosHot).pipe(
              finalize(() => console.log('termino'))
            ).subscribe(data => {
              this.anunciosHot = JSON.parse(data.data);
              loading.dismiss();
            }, err =>{
              console.log('No signal');
            });

        /*this.httpClient.get(localStorage.getItem("URLAPI")+'categorias.php').subscribe( res=> {
          this.posts = res;
        });

        this.apiService.anuncios(this.pagina).subscribe( res=> {
          this.anunciosHot = res;
          for (let post of this.anunciosHot) {
            let foto = this.urlApi+post["link"]+post["fotos"].split(",",1);
            this.fotos = post["fotos"].split(",");
              post.media_url = foto;
          }
          loading.dismiss();
        });*/

        /*this.apiService.anunciosTodos().subscribe(
          resultado => { this.anunciosTodos = resultado; }
        )*/

        let anunciosTodos = this.nativeHttp.get(localStorage.getItem("URLAPI")+'anunciosTodos.php', {}, {'Content-Type': 'application/json'});
        from(anunciosTodos).pipe(
            finalize(() => console.log('termino'))
          ).subscribe(data => {
            this.anunciosTodos = JSON.parse(data.data);
          }, err =>{
            console.log('No signal');
          });
        
    }

    async getAnuncios(){
      let loading = await this.loadinctrl.create({
        message: 'Cargando ...'
      });
            
      await loading.present();

      let anunciosHot = this.nativeHttp.get(localStorage.getItem("URLAPI")+'anuncios.php?pag='+this.pagina, {}, {'Content-Type': 'application/json'});
        from(anunciosHot).pipe(
            finalize(() => console.log('termino'))
          ).subscribe(data => {
            this.anunciosHot = JSON.parse(data.data);
            loading.dismiss();
          }, err =>{
            console.log('No signal');
          });

      /*this.apiService.anuncios(this.pagina).subscribe( res=> {
        this.anunciosHot = res;
        for (let post of this.anunciosHot) {
          let foto = this.urlApi+post["link"]+post["fotos"].split(",",1);
          this.fotos = post["fotos"].split(",");
            post.media_url = foto;
        }
        loading.dismiss();
      });*/

    }

    atrasPag(){
      this.pagina--;
      if(this.pagina <= 0) this.pagina = 1;
      this.getAnuncios();
    }

    sigPag(){
      this.pagina++;
      this.getAnuncios();
    }

    buscarProducto( event ){
      this.textoBuscar = event.target.value;
      let buscar = this.textoBuscar.toLowerCase();

      if(this.textoBuscar.length > 2){
        this.mostrar = false;
        this.pasada = 1;
        this.anunciosHot = this.anunciosTodos.filter( 
          item => {
            return item.nombre_producto.toLowerCase().indexOf( buscar ) > -1 ;
        });
      }else{
        this.pagina = 1;
        this.mostrar = true;

        if(this.pasada == 1){
          this.getAnuncios();
          this.pasada = 0;
        } 
      }      

    } 

    async openBrowser(){
      const noPudo = await this.toastController.create({
        message: 'Error abriendo navegador ...',
        duration: 2000
      });

      let correo = window.btoa(localStorage.getItem('user_login'));
      const browser = this.iab.create('https://mercadoactivo.cl/publicar/?menu=registrar&Y29ycmVvIGVudmlhZG8gZGUgbGEgYXBw='+correo);
     
    }

    guardarToken(token){
      let correo = localStorage.getItem("user_login");
      let guardToken = this.nativeHttp.get(localStorage.getItem("URLAPI")+'token.php?correo='+correo+'&token='+token, {}, {'Content-Type': 'application/json'});
        from(guardToken).pipe(
            finalize(() => console.log('termino'))
          ).subscribe(data => {
            console.log('Guardado');
          }, err =>{
            console.log('No signal');
          })
    }

    habilitarNoti(){
      let correo = localStorage.getItem('user_login');
      let habilitarToken = this.nativeHttp.get(localStorage.getItem("URLAPI")+'habilitarToken.php?correo='+correo, {}, {'Content-Type': 'application/json'});
        from(habilitarToken).pipe(
            finalize(() => console.log('termino'))
          ).subscribe(data => {            
            console.log('Habilitado');
            this.getHabilitado();
          }, err =>{
            console.log('No signal');
          })
    }

    getHabilitado(){
      let correo = localStorage.getItem('user_login');
      let habilitarToken = this.nativeHttp.get(localStorage.getItem("URLAPI")+'habilitado.php?correo='+correo, {}, {'Content-Type': 'application/json'});
        from(habilitarToken).pipe(
            finalize(() => console.log('termino'))
          ).subscribe(data => {            
            this.data = JSON.parse(data.data);
            if(this.data["success"] == true){
              this.habilitado = 1;
            }else{
              this.habilitado = 0;
            }
            console.log('habilitado: '+this.habilitado);
          }, err =>{
            console.log('No signal');
          })
    }

}
