import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../services/api.service';
import { ToastController, LoadingController } from '@ionic/angular';

import { HTTP } from '@ionic-native/http/ngx';
import { finalize } from 'rxjs/operators';
import { from } from 'rxjs';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  id;
  data: any = [];
  list;
  correo;
  urlApi = 'https://mercadoactivo.cl';
  fotos = [];
  arrayFotos = [];

  constructor(  public router: Router, 
                private route: ActivatedRoute, 
                public apiService: ApiService,
                public toastCtrl: ToastController,
                public nativeHttp: HTTP,
                public loadinctrl: LoadingController) 
  { 
    this.id = this.route.snapshot.paramMap.get('postId');
  }

  ngOnInit() {
    this.detalleProducto();
  }

  async detalleProducto(){
    let cargandoInf = await this.loadinctrl.create({
      message: 'Buscando ...'
    });
    await cargandoInf.present();
    
    /*this.apiService.detalleProducto(this.id).subscribe( res => {
      this.list = res;
      for (let post of this.list) {
        let foto = this.urlApi+post["link"]+post["fotos"].split(",",1);
        this.fotos = post["fotos"].split(",");
          post.media_url = foto;
      }
      this.data = this.list;
      cargandoInf.dismiss();
      for(let foto of this.fotos){
        this.arrayFotos.push(this.urlApi+"/storage/products/"+foto);
      }
    });*/

    let detalle = this.nativeHttp.get(localStorage.getItem("URLAPI")+'detalleId.php?producto='+this.id, {}, {'Content-Type': 'application/json'});
        from(detalle).pipe(
            finalize(() => console.log('termino'))
          ).subscribe(data => {
            this.list = JSON.parse(data.data);
            for (let post of this.list) {
              let foto = this.urlApi+post["link"]+post["fotos"].split(",",1);          
              post.media_url = foto;
              this.fotos = post["fotos"].split(",");
            }
            cargandoInf.dismiss();
            for(let foto of this.fotos){
              this.arrayFotos.push(this.urlApi+"/storage/products/"+foto);
            }
            this.data = this.list;
          }, err =>{
            console.log('No signal');
          });

  }

  async comprar(){
    this.correo = localStorage.getItem('user_login');
    const comprado = await this.toastCtrl.create({
      message: 'Compra con éxito !!!',
      duration: 2000
    });
    let comprando = await this.loadinctrl.create({
      message: 'Comprando ...'
    });
    await comprando.present();
    this.apiService.comprarProducto(this.id,this.correo).subscribe( res => {
      comprando.dismiss();
      this.router.navigate(['/tabs/tab2'],{replaceUrl: false});
      comprado.present();
    });
  }

}
