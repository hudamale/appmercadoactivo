import { Component, OnInit } from '@angular/core';

import { ApiService } from '../services/api.service';
import { ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

import { HTTP } from '@ionic-native/http/ngx';
import { finalize } from 'rxjs/operators';
import { Observable, from } from 'rxjs';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-datos',
  templateUrl: './datos.page.html',
  styleUrls: ['./datos.page.scss'],
})
export class DatosPage implements OnInit {

  correo;
  posts;
  urlApi = 'https://mercadoactivo.cl';

  constructor(public loadinctrl: LoadingController, 
    public apiService: ApiService,
    public toastController: ToastController,
    public nativeHttp: HTTP,
    private httpClient: HttpClient,
    public router: Router) { this.correo = localStorage.getItem('user_login'); }

  async ngOnInit() {
    let loading = await this.loadinctrl.create({
      message: 'Cargando ...'
    });
    
    
    await loading.present();

    this.httpClient.get(localStorage.getItem("URLAPI")+'datosUsuario.php?correo='+this.correo).subscribe( res=> {
      this.posts = res;
    });
    loading.dismiss();
    
  }

}
